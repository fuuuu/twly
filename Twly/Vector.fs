﻿module Twly.Math

open System

let piTimes2 = Math.PI * 2.0
let sqrt = Math.Sqrt

let clamp min max value =
    match value with
    | x when x < min -> min
    | x when x > max -> max
    | x -> x

type Vector =
    { X : float
      Y : float
      Z : float }
    member this.Length = sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z)
    member this.Normalized = match this.Length with
                             | 0.0 -> { X = 0.0; Y = 0.0; Z = 0.0 }
                             | _ -> { X = this.X / this.Length; Y = this.Y / this.Length; Z = this.Z / this.Length }
    static member (+) (v1 : Vector, v2 : Vector) =
        { X = v1.X + v2.X; Y = v1.Y + v2.Y; Z = v1.Z + v2.Z }
    static member (*) (v1 : Vector, v2 : Vector) =
        { X = v1.Y * v2.Z - v1.Z * v2.Y; Y = v1.Z * v2.X - v1.X * v2.Z; Z = v1.X * v2.Y - v1.Y * v2.X }
    static member (*) (v : Vector, n) =
        { X = v.X * n; Y = v.Y * n; Z = v.Z * n }

let ZeroVector = { X = 0.0; Y = 0.0; Z = 0.0 }
