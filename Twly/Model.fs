﻿module Twly.Model

open FSharp.Data
open SFML.Graphics
open Tao.OpenGl
open Twly.Math

[<Literal>]
let sampleFile = "/home/janno/Work/Twly/Twly/Media/Models/Model.dae"

type ColladaFile = XmlProvider<sampleFile>

type MeshMaterial = {
    Name : string
    Ambient : Vector
    Diffuse : Vector
    Specular : Vector
    Shininess : float
    Transparency : float
    Texture : Texture option
}

type MeshVertex = {
    Position : Vector
    Normal : Vector option
    Texture : SFML.Window.Vector2f option
}

type MeshGroup = {
    Material : MeshMaterial option
    Vertices : MeshVertex list
}

type MeshObject = {
    Name : string
    Groups : MeshGroup list
}

type Mesh = {
    Name : string
    Objects : MeshObject list
}

let drawMesh (mesh : Mesh) =
    let drawGroup (meshGroup : MeshGroup) =
        Gl.glEnable(Gl.GL_CULL_FACE)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glDepthFunc(Gl.GL_LEQUAL)
        match meshGroup.Material with
        | Some material ->
            match material.Texture with
            | Some texture -> Texture.Bind(texture)
            | _ -> ()
        | _ -> ()
        Gl.glBegin(Gl.GL_TRIANGLES)
        meshGroup.Vertices |> List.iter (fun v ->
            match v.Normal with
            | Some n -> Gl.glNormal3d(n.X, n.Y, n.Z)
            | _ -> ()
            match v.Texture with
            | Some t -> Gl.glTexCoord2f(t.X, t.Y)
            | _ -> ()
            Gl.glVertex3d(v.Position.X, v.Position.Y, v.Position.Z)
        )
        Gl.glEnd()
    let drawObject (meshObject : MeshObject) =
        meshObject.Groups |> List.iter drawGroup
    mesh.Objects |> List.iter drawObject

let loadMesh (fileName : string) =
    // TODO : Types generated from XSD schema would be nice here
    let dataFile = ColladaFile.Load(fileName)

    let polylistInputs = dataFile.LibraryGeometries.Geometry.Mesh.Polylist.Inputs
    let vertexSource = polylistInputs |> Array.find (fun i -> i.Semantic = "VERTEX")
    let normalSource = polylistInputs |> Array.tryFind (fun i -> i.Semantic = "NORMAL")
    let numInputs = polylistInputs.Length

    let sources = dataFile.LibraryGeometries.Geometry.Mesh.Sources
    let vertices = sources |> Array.find (fun s -> (sprintf "#%s" s.Id) = "#Cube-mesh-positions") //vertexSource.Source)
    let normals = match normalSource with
                  | Some source -> Some (sources |> Array.find (fun s -> (sprintf "#%s" s.Id) = source.Source))
                  | _ -> None

    let rec toVectors (list : string list) =
        match list with
        | [] -> []
        //| x::y::z::list -> { X = float x; Y = float y; Z = float z } :: (toVectors list)
        | x::z::y::list -> { X = float x; Y = float y; Z = -(float z) } :: (toVectors list)
        | _ -> failwith "Invalid number of coordinates."

    let positionVectors = vertices.FloatArray.Value.Split(' ') |> Array.toList |> toVectors |> List.toArray
    let normalVectors = match normals with
                        | Some n -> Some (n.FloatArray.Value.Split(' ') |> Array.toList |> toVectors |> List.toArray)
                        | _ -> None

    let faces = dataFile.LibraryGeometries.Geometry.Mesh.Polylist.P.Split(' ') |> Array.toList
    let rec toFaces (list : string list) =
        match list with
        | [] -> []
        | pos :: nor :: _ :: list -> { Position = positionVectors.[int pos]
                                       Normal = match normalVectors with | Some v -> Some (v.[int nor]) | _ -> None
                                       Texture = None } :: (toFaces list)
        | _ -> failwith "Invalid number of vertices"

    let group = { Material = None; Vertices = (toFaces faces) }
    let meshObject = { Name = dataFile.LibraryGeometries.Geometry.Name; Groups = [ group ] }
    { Name = "Cube"; Objects = [ meshObject ] }
