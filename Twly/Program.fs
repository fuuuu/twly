﻿module Twly.Main

open SFML.Graphics
open SFML.Window
open System
open System.Diagnostics
open System.IO
open System.Reflection
open Tao.OpenGl
open Twly.Screen

[<EntryPoint>]
let main args =
    let settings = new ContextSettings(24u, 8u)

    use window = new RenderWindow((new VideoMode(800u, 600u)), (sprintf "Twly v.%s" Twly.AssemblyInfo.assemblyVersion), Styles.Default, settings)

    window.SetVerticalSyncEnabled(true)
    window.SetFramerateLimit(60u)
    window.Resized.AddHandler (fun _ e -> Gl.glViewport(0, 0, int e.Width, int e.Height))

    async {
        do! IntroScreen.run window
        do! MenuScreen.run window
    } |> Async.StartImmediate

    0
