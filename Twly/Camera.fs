﻿namespace Twly.Accessories

open Tao.OpenGl
open Twly.Math

module Camera =
    type Movement = Walk | Fly
    type PitchMode = Regular | Inverse

    let UpDirection = { X = 0.0; Y = 1.0; Z = 0.0 }

    type CameraSetup =
        { Position : Vector
          Velocity : Vector
          Pitch : float
          Yaw : float
          Movement : Movement
          PitchMode : PitchMode }
        member this.Direction =
            let sinPitch, sinYaw = System.Math.Sin(this.Pitch), System.Math.Sin(this.Yaw)
            let cosPitch, cosYaw = System.Math.Cos(this.Pitch), System.Math.Cos(this.Yaw)
            { X = cosPitch * sinYaw; Y = sinPitch; Z = cosPitch * cosYaw }.Normalized
        member this.Right =
            (UpDirection * this.Direction).Normalized

    let InitialCamera = {
        Position = { X = 0.0; Y = 1.8; Z = 0.0 }
        Velocity = ZeroVector
        Pitch = 0.0
        Yaw = 0.0
        Movement = Walk
        PitchMode = Regular
    }

    let maxPitch = 1.0
    let maxVelocity = 1.0

    let moveCamera units (camera : CameraSetup) =
        match units with
        | 0.0 -> camera
        | _ ->
            let direction = match camera.Movement with
                            | Walk -> { camera.Direction with Y = 0.0 }.Normalized
                            | _ -> camera.Direction
            { camera with Velocity = direction * units }

    let strafeCamera units (camera : CameraSetup) =
        match units with
        | 0.0 -> camera
        | _ -> { camera with Velocity = camera.Right * units }

    let pitchCamera degrees (camera : CameraSetup) =
        let amount = match camera.PitchMode with
                     | Inverse -> -degrees
                     | _ -> degrees
        { camera with Pitch = clamp -maxPitch maxPitch camera.Pitch + amount }

    let yawCamera degrees (camera : CameraSetup) =
        { camera with Yaw = (camera.Yaw - degrees) % piTimes2 }

    let updateCamera (camera : CameraSetup) =
        let velocity =
            match camera.Velocity.Length with
            | len when len > maxVelocity -> camera.Velocity.Normalized * maxVelocity
            | _ -> camera.Velocity
        { camera with Position = camera.Position + velocity
                      Velocity = ZeroVector }

    let applyCamera (camera : CameraSetup) =
        let lookAt = camera.Position + camera.Direction
        Glu.gluLookAt(camera.Position.X, camera.Position.Y, camera.Position.Z, lookAt.X, lookAt.Y, lookAt.Z, 0.0, 1.0, 0.0)
