﻿namespace Twly.Screen

open SFML.Graphics
open SFML.Window
open System
open System.Diagnostics
open System.IO
open System.Reflection
open Tao.OpenGl
open Twly.Accessories.Camera

module IntroScreen =
    let run (window : RenderWindow) = async {
        use unsubClose = window.KeyPressed
                         |> Observable.filter (fun e -> e.Code = Keyboard.Key.Escape)
                         |> Observable.map (fun e -> e :> EventArgs)
                         |> Observable.merge window.Closed
                         |> Observable.subscribe (fun _ -> window.Close())

        // TODO!
        ()
    }

module MenuScreen =
    type InputState =
        | StrafeCamera of float
        | PitchCamera of float
        | CloseWindow
        | ResizeWindow of System.Drawing.Size
        | MoveCamera of float
        | YawCamera of float
        | ToggleLights

    type InputStateHandler (window : RenderWindow) =
        let mutable mouseOrigin = new Vector2i(int (window.Size.X / 2u), int (window.Size.Y / 2u))

        do
            window.SetMouseCursorVisible(false)
            Mouse.SetPosition(mouseOrigin, window)

        let mutable state = new System.Collections.Generic.List<InputState>()

        let unsubscribeAll = [
            window.Closed |> Observable.subscribe (fun e -> state.Add CloseWindow)

            window.KeyPressed |> Observable.subscribe (fun e ->
                match e.Code with
                | Keyboard.Key.Escape -> state.Add CloseWindow
                | Keyboard.Key.L -> state.Add ToggleLights
                | _ -> ()
            )

            window.Resized |> Observable.subscribe (fun e ->
                mouseOrigin <- new Vector2i(int (e.Width / 2u), int (e.Height / 2u))
                state.Add (ResizeWindow(new System.Drawing.Size(int e.Width, int e.Height)))
            )

            window.MouseMoved |> Observable.subscribe (fun e ->
                let dx, dy = e.X - mouseOrigin.X, e.Y - mouseOrigin.Y
                match dx, dy with
                | 0, 0 -> ()
                | _ ->
                    if dx <> 0 then
                        state.Add (YawCamera (float dx))
                    if dy <> 0 then
                        state.Add (PitchCamera (float dy))
                    Mouse.SetPosition(mouseOrigin, window)
            )
        ]

        let isPressed (keys : Keyboard.Key list) =
            keys |> List.exists Keyboard.IsKeyPressed

        let keyDirection keyPos keyNeg =
            let pos = match Keyboard.IsKeyPressed keyPos with | true -> 1 | _ -> 0
            let neg = match Keyboard.IsKeyPressed keyNeg with | true -> 1 | _ -> 0
            match (pos - neg) with
            | 0 -> None
            | x -> Some(float x)

        let applyDirection keyPos keyNeg magnitude createState =
            match keyDirection keyPos keyNeg with
            | Some amount -> state.Add (createState (magnitude * amount))
            | _ -> ()

        member this.GetInputState () : InputState list =
            state.Clear()
            window.DispatchEvents()

            applyDirection Keyboard.Key.Left Keyboard.Key.Right 2.0 (fun x -> StrafeCamera x)
            applyDirection Keyboard.Key.Up Keyboard.Key.Down 2.0 (fun x -> MoveCamera x)
            applyDirection Keyboard.Key.W Keyboard.Key.S 2.0 (fun x -> PitchCamera x)
            applyDirection Keyboard.Key.D Keyboard.Key.A 2.0 (fun x -> YawCamera x)

            state |> Seq.toList

        interface System.IDisposable with
            member this.Dispose() =
                unsubscribeAll |> List.iter (fun x -> x.Dispose())

    type GameState = {
        Perspective : float
        FrameTime : float
        Camera : CameraSetup
        LightsOn : bool
    }

    let run (window : RenderWindow) = async {
        //let shape = new CircleShape(100.0f, FillColor = Color.Green)

        let stopwatch = new Stopwatch()
        stopwatch.Start()

        let executingAssembly = Assembly.GetExecutingAssembly().Location
        let rootPath = Path.GetDirectoryName(executingAssembly)

        let (@@) part1 part2 = Path.Combine(part1, part2)
        let font = new Font(rootPath @@ "Media" @@ "Fonts" @@ "MesloLGL-Bold.ttf")
        let debugText = new Text("", font, Color = new Color(0uy, 0uy, 0uy, 255uy), CharacterSize = 11u)

        let model = Twly.Model.loadMesh (rootPath @@ "Media" @@ "Models" @@ "Model.dae")

        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glShadeModel(Gl.GL_SMOOTH)
        Gl.glClearColor(0.39f, 0.58f, 0.93f, 1.0f)
        Gl.glClearDepth(1.0)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glDepthFunc(Gl.GL_LEQUAL)
        Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST)
        Gl.glEnable(Gl.GL_CULL_FACE)
        Gl.glCullFace(Gl.GL_BACK)
        Gl.glEnable(Gl.GL_LIGHT0)
        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, [| 0.5f; 0.5f; 0.5f; 1.0f |])
        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, [| 1.0f; 1.0f; 1.0f; 1.0f |])

        let ish = new InputStateHandler(window)

        let rec updateAndRenderFrame (gameState : GameState) =
            let rec renderFrame (gameState : GameState) =
                Gl.glClear(Gl.GL_COLOR_BUFFER_BIT ||| Gl.GL_DEPTH_BUFFER_BIT)
                Gl.glMatrixMode(Gl.GL_PROJECTION)
                Gl.glLoadIdentity()
                Glu.gluPerspective(45.0, gameState.Perspective, 0.1, 100.0)
                Gl.glMatrixMode(Gl.GL_MODELVIEW)
                Gl.glLoadIdentity()
                applyCamera gameState.Camera

                if gameState.LightsOn then
                    Gl.glEnable(Gl.GL_LIGHTING)
                    Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, [| 4.56f; 2.30f; 0.50f; 1.0f |])

                Twly.Model.drawMesh model
                window.ResetGLStates()
                //window.Draw(shape)
                window.Draw(debugText)
                window.Display()
                gameState

            let rec updateFrame (gameState : GameState) (inputState : InputState list) =
                match inputState with
                | [] ->
                    let camera = updateCamera gameState.Camera
                    debugText.DisplayedString <- (sprintf "dt: %.3f; pos: (%.2f; %.2f; %.2f)" gameState.FrameTime camera.Position.X camera.Position.Y camera.Position.Z)
                    renderFrame { gameState with Camera = camera }
                | CloseWindow :: xs ->
                    window.Close()
                    gameState
                | ResizeWindow (size) :: xs ->
                    Gl.glViewport(0, 0, size.Width, size.Height)
                    debugText.Position <- new Vector2f(0.0f, float32 (size.Height - 20))
                    updateFrame { gameState with Perspective = (float size.Width) / (float size.Height) } xs
                | StrafeCamera (amount) :: xs ->
                    updateFrame { gameState with Camera = strafeCamera (amount * gameState.FrameTime) gameState.Camera } xs
                | PitchCamera (amount) :: xs ->
                    updateFrame { gameState with Camera = pitchCamera (amount * gameState.FrameTime) gameState.Camera } xs
                | YawCamera(amount)::xs ->
                    updateFrame { gameState with Camera = yawCamera (amount * gameState.FrameTime) gameState.Camera } xs
                | MoveCamera(amount)::xs ->
                    updateFrame { gameState with Camera = moveCamera (amount * gameState.FrameTime) gameState.Camera } xs
                | ToggleLights::xs ->
                    updateFrame { gameState with LightsOn = not gameState.LightsOn } xs
                | x :: xs -> updateFrame gameState xs

            updateFrame gameState (ish.GetInputState())

        let rec gameLoop (gameState : GameState) =
            let newFrameTime = (float stopwatch.ElapsedMilliseconds) / 1000.0
            stopwatch.Restart()
        
            if window.IsOpen() then
                let gameState = updateAndRenderFrame { gameState with FrameTime = newFrameTime }

                gameLoop gameState

        gameLoop { FrameTime = 0.0; Perspective = 1.3; Camera = InitialCamera; LightsOn = true }
    }
