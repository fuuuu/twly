﻿module Twly.AssemblyInfo

open System.Reflection
open System.Runtime.CompilerServices

[<Literal>]
let assemblyVersion = "1.0.0.0"

[<assembly: AssemblyTitle("Twly")>]
[<assembly: AssemblyDescription("Apartment simulation")>]
[<assembly: AssemblyConfiguration("")>]
[<assembly: AssemblyCompany("")>]
[<assembly: AssemblyProduct("")>]
[<assembly: AssemblyCopyright("fuuuu")>]
[<assembly: AssemblyTrademark("")>]
[<assembly: AssemblyVersion(assemblyVersion)>]

()
